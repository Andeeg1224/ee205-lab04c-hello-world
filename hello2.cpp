///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World C++
///
/// @file hello2.cpp
/// @version 1.0
///
/// This is a Hello world program using namespace std;
///
/// @author @Andee Gary  <@todo yourMail@hawaii.edu>
/// @brief  Lab 04c - Hello World  - EE 205 - Spr 2021
/// @date   @ 10_Feb_2021
///
///////////////////////////////////////////////////////////////////////////////

#include<iostream>

int main(){
   std::cout <<"Hello World" << std::endl;
}


